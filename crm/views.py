from django.shortcuts import render, HttpResponse

# Create your views here.

from rbac.models import User
from rbac.service.permission import initial_session

def login(request):
    if request.method == "POST":
        user = request.POST.get("user")
        pwd = request.POST.get("pwd")

        user_obj = User.objects.filter(name=user, pwd=pwd).first()
        print("当前登录的用户：", user_obj)
        print(user_obj.pk)

        if user_obj:
            request.session['user_id'] = user_obj.pk

            #注册权限到session中
            initial_session(user_obj, request)
            return HttpResponse("登录成功")
            # return redirect("/index/")

    return render(request, "login.html")
