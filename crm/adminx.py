#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Author: Henson

from .models import *
import xadmin
from xadmin import views
# 基础设置
class BaseSetting(object):
    enable_themes = True    # 使用主题
    use_bootswatch = True

# 全局设置
class GlobalSettings(object):
    site_title = 'CRM后台管理'  # 标题
    site_footer = 'CRM后台管理（henson）'  # 页尾
    site_url = '/'
    menu_style = 'accordion'  # 设置左侧菜单  折叠样式


xadmin.site.register(views.CommAdminView, GlobalSettings)
xadmin.site.register(views.BaseAdminView, BaseSetting)


class CustomerAdmin(object):
    list_display = ['id','name','gender','education','salary']
    ordering = ('id',)
    list_editable = ['salary',]
    data_charts = {
        "user_count": {"title": "客户薪资", "x-field": "id", "y-field": ("salary",)}
    }


xadmin.site.register(Department)
xadmin.site.register(UserInfo)
xadmin.site.register(Course)
xadmin.site.register(School)
xadmin.site.register(ClassList)
xadmin.site.register(Customer, CustomerAdmin)
xadmin.site.register(ConsultRecord)
xadmin.site.register(Student)
xadmin.site.register(CourseRecord)
xadmin.site.register(StudyRecord)