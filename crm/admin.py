from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Department)
admin.site.register(UserInfo)
admin.site.register(Course)
admin.site.register(School)
admin.site.register(ClassList)
admin.site.register(Customer)
admin.site.register(ConsultRecord)
admin.site.register(Student)
admin.site.register(CourseRecord)
admin.site.register(StudyRecord)