# **web_crm**
***
#### 项目介绍
- CRM + xadmin - 客户关系管理软件
***

> * python3.6
> * django 1.11.1
> * xadmin 
 
### **crm 客户关系管理软件**
> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/030311_9f392b92_1240823.png "屏幕截图.png")
 

#### 效果图：
> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/204139_fece7630_1240823.png "屏幕截图.png")
> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/034355_40319a1e_1240823.png "屏幕截图.png")

#### 不同的权限显示不同的页面
> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/034402_d907094d_1240823.png "屏幕截图.png")

#### 分配权限 

>![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/034619_3b59b9e6_1240823.png "屏幕截图.png")

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/034638_fbd79907_1240823.png "屏幕截图.png")

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0814/034647_35efaf89_1240823.png "屏幕截图.png")

####  xadmin后台
> ![输入图片说明](https://images.gitee.com/uploads/images/2018/0818/174455_316e6d4c_1240823.png "屏幕截图.png")