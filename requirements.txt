httplib2==0.9.2
Django==1.11.1
six==1.11.0
xlwt==1.3.0
future==0.16.0
django_crispy_forms==1.7.2
django_formtools==2.1
django_reversion==3.0.0
xlsxwriter==1.0.7
