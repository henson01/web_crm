from django.test import TestCase

# Create your tests here.

# li = [
#     {'permission__url': '/users/',
#     'permission__group_id': 1,
#     'permission__action': 'list'},
#
#     {'permission__url': '/users/add/',
#     'permission__group_id': 1,
#     'permission__action': 'add'},
#
#
#     {'permission__url': '/roles/',
#     'permission__group_id': 2,
#     'permission__action': 'list'},
#
#
#     {'permission__url': '/roles/add',
#      'permission__group_id': 2,
#      'permission__action': 'add'}
# ]
#
# """{
#         1:{
#             "url":['/users/','/users/add','/users/delete/(\\d+)/','/users/edit/(\\d+)']
#             "action":['list','add','delete','edit']
#         }
#
#         2：{
#             "url":['/roles/']
#             "action":['list']
#         }
#     }
# """
# """
# {'permission__url': '/users/', 'permission__group_id': 1, 'permission__action': 'list'}
# {'permission__url': '/users/add/', 'permission__group_id': 1, 'permission__action': 'add'}
# {'permission__url': '/roles/', 'permission__group_id': 2, 'permission__action': 'list'}
#
# """
# temp ={}
# for i in li:
#     if i['permission__group_id'] in temp.keys():
#         temp[i['permission__group_id']]['url'].append(i['permission__url'])
#         temp[i['permission__group_id']]['action'].append(i['permission__action'])
#     else:
#         temp[i['permission__group_id']] = {"url":[i['permission__url']],"action":[i["permission__action"]]}
#
# # print(temp)
#
# # {1: {'url': ['/users/', '/users/add/'], 'action': ['list', 'add']},
# # 2: {'url': ['/roles/', '/roles/add'], 'action': ['list', 'add']}}
#
#
# # 循环
#
# # dict = {1: {'urls': ['/users/', '/users/add/', '/users/delete/(\\d+)/', '/users/edit/(\\d+)/'],
# #     'actions': ['list', 'add', 'delete', 'edit']},
# #     2: {'urls': ['/roles/'],
# #     'actions': ['list']}}
# #
# # for item in dict.values():
# #     print(item['urls'])
#
# # 补充知识点
#
# class Person:
#     def __init__(self,name):
#         self.name = name
#
#     def add(self):
#         return True
#
#     def delete(self):
#         return False
#
# alex = Person('alex')
#
# alex.age = 12
#
# print(alex.name)
# print(alex.age)
# print(alex.add)
# print(alex.delete)
# if alex.add:
#     print('1')
# if alex.delete:
#     print('2')
#
#
